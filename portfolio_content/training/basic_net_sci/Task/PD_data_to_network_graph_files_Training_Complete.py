
# coding: utf-8

# # Preparing Personality Disorder data as input files for Gephi (an open source network analysis & visualisation software tool)

# ## Overview of the code
# 
# ### Code functionality
# This code takes 1 input file (the Personality Disorder dataset) and creates 3 output files (the input files required by Gephi to create a network graph).
# 
# The network graph is a visual representation of the order in which Personality Disorder clients access services: the nodes of the network are the individual services (the Ward Teams); the edges (links between the nodes) represents the order (chronological by referral date) in which a patient has accessed the services.
# 
# It is possible to either include all of the PD data in the network graph, or to include only a subset of the PD data in the network.  This code allows for a full network, or for a single patient.  Please refer to the full version of the code that can create all subsets.
# 
# Gephi requires two input files in order to create a network graph:
# 
# 1. Node file: 
# The <em>Node</em> file contains information (categorical or continuous) about each node, stored per row.  In this example the Node file will contain: a unique numerical node ID; the node name (the Ward Team); the length of stay at the Ward Team (mean and median); the setting (community, inpatient, out of area, or mixture for the instances when a node represents a combination of many WardTeams).
# 
# 2. Edge file: 
# The <em>Edge</em> file contains data about the links between two nodes, stored per row.  A link is defined by the Source Node ID and the Target Node ID (corresponding to the ID used in the Node file).  The type of each link is defined as either directed or undirected (for our case the links are all directed, because a patient accesses a service in a particular order).  The weight is the frequency that the link has been used.  A unique ID is provided for each edge.
# 
# A third output file is created by this code (but not used as an input file for Gephi):
# 
# 3. Service movement file: 
# The <em>Service movement</em> file is an interime working file that contains a matrix of the frequency of use of the links from all services to all other services (a row and column per service [Ward Team]).  The <em>Edge</em> file is created driectly from the <em>Service movement</em> file. 
# 
# ### Code Structure
# In addition to the main code, the code is divided into 5 functions (a function is defined by beginning with <em>def</em> and ending with <em>return</em>. It is worth creating a function for script that is used multiple times, or to break code down into smaller chunks. 
# 
# Three of the functions create and output each of the three output files: (1) <em>output_SM_file()</em>, (2) <em>output_Edge_file()</em>, (3) <em>output_Node_file()</em>.
# 
# Function (4) <em>create_output_files()</em> runs sequentially through the three functions that create and output each of the three output files:<em>output_SM_file()</em>, <em>output_Edge_file()</em>, <em>output_Node_file()</em>.
# 
# Function (5) <em>categorise_columns()</em> adds the numerical representation of the WardTeam column to the provided pandas dataframe.  The unique ID runs from 1 to n.  When this column is used as the row and colmun referrence for the matrix servMove, need to always -1 from the ID so it runs from 0 to n-1. 
# 
# The main code reads in the Personality Disorder dataset as a pandas dataset.  The data is cleaned, additional columns are added and the data is sorted. A replica of the pandas dataset is created that represents all of the Out Of Area Ward Teams as a single node.  Then the unique IDs for the Ward Teams are added to the selected data, and passed to function <em>create_output_files()</em> that calls the three functions in turn to create the output files.
# 
# The data that are passed between the functions is held in a directory <em>file_output_info</em>.
# 
# # EXERCISES 
# Throughout the code, lines of code have been removed and a description of what functionality is required is given.  Whenever you see #INSERT YOUR CODE HERE within a code cell, please attempt to write the missing code.
# 
# First let's import our libraries.

# ## Import the modules
# 
# # EXERCISE 1.
# 
# We need to use two modules: pandas & numpy.
# 
# Import them and use conventional shortnames.

# In[ ]:


import pandas as pd
import numpy as np


# ## Set up dictionary
# 
# Create a dictionary <em>file_output_info</em> that initially holds 6 elements to specify the folder location that contains the data (input and output) and the filenames for the input file and output files.  Two additional elements will be included in this directory and are changed each time look at a different set of data.
# 
# Dictionary <em>file_output_info</em> contains 8 elements:
# 
# (These 6 are constant throughout the program and take their value from the user)
# 
# "FOLDER" : The subfolder where the input and output data are located
# 
# "FILESTART" : The filename of the input data, this is used for the start of the output filenames
# 
# "FILEENDSM" : End of the output filename for the <em>Service movement</em> data
# 
# "FILEENDEDGE" : End of the output filename for the <em>Edge</em> data
# 
# "FILEENDNODE" : End of the output filename for the <em>Node</em> data
# 
# "FILEEX" : File extension for the input and output files
# 
# (These 2 change for each set of data)
# 
# "FILEMIDDLE" : Middle of the output filename to specify the subgroup
# 
# "DATA_SG" : Prepared PD dataset in terms of the subgroup, for the create_output_files function

# In[ ]:


file_output_info = {"FOLDER" : 'Data/', 
                    "FILESTART" : 'PD_Data_HSMA', 
                    "FILEENDSM" : '_SM', 
                    "FILEENDEDGE" : '_edgeList', 
                    "FILEENDNODE" : '_nodeList', 
                    "FILEEX" : '.csv'}
print(file_output_info)


# ## Read in the data
# Read in the personality disorder admission data into a pandas dataframe.  This contains dated admissions to a specific ward team.  
# 
# <em>Note: needed to add low_memory=False to remove the error: "DtypeWarning: Columns (11,12) have mixed types. Specify dtype option on import or set low_memory=False."</em>

# In[ ]:


DATA = pd.read_csv(file_output_info['FOLDER'] + 
                   str(file_output_info['FILESTART']) +
                   str(file_output_info['FILEEX']), 
                   low_memory = False)
print(DATA.head)


# ## Clean the data: how to represent the NaN values
# 
# For column ReferralDischarge, replace "Nan" with the date the data was acquired (18/02/2018).  This is due to that service use being ongoing at the time the data was accessed.
# 
# Replace all other "Nan" with "None".
# 
# Remove rows with no ReferralDate
# 
# # EXERCISE 2
# 
# Replace all other "NaN" with "None"

# In[ ]:


#Replace ReferralDischange NaN with the date received the data
DATA.ReferralDischarge.replace(np.nan,"18/02/2018", inplace = True)

#EXERCISE 2
#Replace all other NaN with "None"
DATA.replace(np.nan,"None", inplace = True)

#Remove instances without a ReferralDate
DATA = DATA[DATA.ReferralDate != "None"]

print(DATA.head)


# ## Add column: Calculate length of stay
# 
# Change the format of the date columns (needed to calculate the length of stay, LoS).
# 
# Add a column (LoSdays) and store the calculated Length of Stay in days. 

# In[ ]:


#  Changes the format of the columns that store a date, to be a date format
DATA['ReferralDate'] = pd.to_datetime(DATA['ReferralDate'],
                                      format = "%d/%m/%Y")
DATA['ReferralDischarge'] = pd.to_datetime(DATA['ReferralDischarge'], 
                                           format = "%d/%m/%Y")

#  Calculate the length of stay (LoS).  Use variable type for LoSdays as day (float64)
DATA['LoSdays'] = (DATA.ReferralDischarge - 
                   DATA.ReferralDate).astype('timedelta64[D]')
print(DATA.columns)
print(DATA.LoSdays.iloc[0:10])


# ## Remove negative length of stay durations
# 
# Remove any instance that has a negative length of stay (column 'LoSdays')
# 
# # EXERCISE 3
# 
# Only keep the rows in the pandas dataframe (DATA) that have a LoSdays greater than or equal to 0

# In[ ]:


#EXERCISE 3
# Remove any instance that has a negative LoS
DATA = DATA[DATA.LoSdays >= 0]


# ## Sort the data
# 
# Sort the data into blocks for each client ('ClientID') with chronological referral date ('ReferralDate').  Note: for dates to be sorted chronologically, need to be in date format, otherwise will order by day number first.
# 
# # EXERCISE 4
# 
# Sort the pandas dataframe (DATA): ClientID ascending & ReferralDate ascending

# In[ ]:


print(DATA.iloc[0:28,0:2])
                
#EXERCISE 4
#Sort the data by Client and order their admissions chronologically on the date 
#the service was accessed (ReferralDate)
DATA = DATA.sort_values(['ClientID','ReferralDate'], ascending = [True,True])
                
print(DATA.iloc[0:28,0:2])


# ## Create a replica of the pandas dataframe (DATA) with one Out Of Area node
# 
# Create a separate Pandas dataframe that represents all of the Out Of Area nodes as a single node (by changing all of the names of the Ward Teams with a Setting - OOA, to "All OOA Services").
# 
# It is not good practice to change the value of a slice in a pandas dataframe.
# 
# Instead create the new values in either a list, or a numpy array, and then set a column in the pandas dataframe to this object.
# 
# Here create two numpy arrays: wardteam_np & setting_np which take the values of corresponding columns of the pandas dataframe.  Change the values wardteam_np when setting_np = OOA.  Delete the existing column of WardTeam and create it with the new values in wardteam_np.

# In[ ]:


#take a deep copy of the pandas dataframe DATA
DATA_OneOOA = DATA.copy(deep = True)
print(DATA_OneOOA.WardTeam.iloc[85:105])

#create 2 numpy arrays which contain a column of data from the pandas dataframe
wardteam_np = DATA_OneOOA.WardTeam.values
setting_np = DATA_OneOOA.Setting.values

#create a mask for the location of the OOA WardTeams (based on 'Setting')
mask = setting_np == 'OOA'

#use mask on WardTeam names to replace all OOA WardTeams with 'All OOA services'
wardteam_np[mask] = str('All OOA services')

#remove existing column & replace with new data from numpy array wardteam_np
del DATA_OneOOA['WardTeam']
DATA_OneOOA['WardTeam'] = wardteam_np
print(DATA_OneOOA.WardTeam.iloc[85:105])


# # END OF DATA CLEANING
# 
# ## Create a network for the whole dataset
# 
# ### Using one node to represent all of the Out Of Area Ward Teams
# There are a large number of OOA Ward Teams that are not used as frequently as those in the region.  Including the individual OOA ward teams can add noise to the network (lots of individual nodes) when the important information is that an OOA ward team was used, and not necessarily which OOA ward team.
# 
# The choice of pandas dataframe (DATA or DATA_OneOOA) is stored in dictionary <em>file_output_info</em>, element 'DATA_SG'.
# 
# # EXERCISE 5
# 
# Want to use pandas dataframe DATA_OneOOA.  
# Create a new element 'DATA_SG' within the dictionary <em>file_output_info</em> to store a deep copy of the pandas dataframe DATA_OneOOA.

# In[ ]:


#EXERCISE 5
#Deep copy of DATA_OneOOA into dictionary element DATA_SG
file_output_info["DATA_SG"] = DATA_OneOOA.copy(deep = True)

print(file_output_info)


# Dictionary <em>file_output_info</em> element 'FILEMIDDLE' stores a string to describe the dataset included in the output files.  For this case suggest including the string "_OneOOA" to represent the choice of representaing all the OOA ward teams as a single node.

# In[ ]:


file_output_info ['FILEMIDDLE'] = '_OneOOA'
print(file_output_info)


# ## Datasets are now complete.
# All the preparation of the data has been done.  Next is how to convert this data into the format necessary for Gephi.
# 
# ## Represent the Ward Teams as a numerical category code
# 
# We will use the next block of code again, so have put it into a function so that it can be used multiple times.
# 
# Functions need to appear before they are called, so here we define a function that will be called by the cell that follows.
# 
# Function <em>categorise_columns()</em> adds the numerical representation of Ward Teams present in the pandas dataframe by adding 2 columns: wardTeamCat & wardTeamCatCode.
# 
# The function is passed a directory that contains a pandas dataframe in element DATA_SG (the PD data to be represented as a network, so either the full dataset, or a subgroup).
# 
# The function and adds two columns in order to format the WardTeam column (currently an object) into a unique numerical ID that can be used as the nodes reference ID in the output files. 
# 
# This is done in two stages: 
# 1). Create a new column WardTeamCat: converts the object to a categorical variable.
# 2). Create a new column WardTeamCatCode: converts the categorical variable to the unique numerical ID.
# 
# Function returns the directory <em>file_output_info</em> with the updated element 'DATA_SG'.

# In[ ]:


def categorise_columns(file_output_info):
    """Converts the WardTeam column into a numerical ID field
    Adds 2 new columns to pandas dataframe (stored in the element 'DATA_SG')
    of the dictionary file_output_info"""
    
    #To have categories represented as numbers, first need data type as categories  
    file_output_info['DATA_SG']['wardTeamCat'] =         file_output_info['DATA_SG']['WardTeam'].astype('category')
    
    #Then store the categories as the numerical codes
    file_output_info['DATA_SG']['wardTeamCatCode'] =         file_output_info['DATA_SG']['wardTeamCat'].cat.codes
    
    return file_output_info


# In[ ]:


file_output_info = categorise_columns(file_output_info)
print(file_output_info['DATA_SG'].columns)
print(file_output_info['DATA_SG'].WardTeam.iloc[0:20])
print(file_output_info['DATA_SG'].wardTeamCatCode.iloc[0:20])


# ## Functions to create the output files from the information stored in dictionary file_output_info
# 
# The following block of code (in the 4 cells below) will be used again, and so each is put into a function.
# 
# Functions need to appear before they are called, so here we define the 4 functions that will be called by the cell that follows.
# 
# Function <em>create_output_files()</em> calls the series of three functions to create the three output files.
# 
# This function is called for each subset of data going to be represented in a network.  It gets passed a dictionary containing a Pandas dataframe of the PD data (<em>DATA_SG</em>) and the variables containing the filename and file location.

# In[ ]:


def create_output_files(file_output_info):
    
    """Called for each subset of data going to be represented in a network. 
    
    Functions are passed a dictionary (file_output_info).
    
    Dictionary contains:
    "DATA_SG" :  Pandas dataframe of the PD data (updated for each subgroup)
    "FOLDER" : String containing the data folder name, within python script folder
    "FILESTART" : String containing the input file name.  Used for output file names 
    "FILEMIDDLE : String containing the subgroup specific part of the output file 
                    name (updated for each subgroup of data)
    "FILEENDSM" : String containing the end of the ServMove output file name
    "FILEENDEDGE" : String containing the end of the Edge output file name
    "FILEENDNODE" : String containing the end of the Node output file name
    "FILEEX" : String containing the file extension (both input and output)
        
    Calls series of three functions to create the three output files
    """
    
    #create service movement matrix
    servMove = output_SM_file(file_output_info)
    
    #create edge file from the service movement matrix
    output_Edge_file(servMove, file_output_info)
    
    #create node file
    output_Node_file(file_output_info)
    return


# Function <em>output_SM_file()</em> is the first in a series of 3 functions that outputs a file.
# 
# It gets passed a dictionary <em>file_output_info</em> containing a Pandas dataframe of the PD data (in element <em>DATA_SG</em>), from which to create the network.  This data is grouped by patient and ordered chronologically on the date the services they accessed (.ReferralDate).
# 
# This function returns a NumPy array (<em>servMove</em>) with a row and column for each unique WardTeam in the passed Pandas dataframe.  The values stored are the frequency a patient chronologically used a service following another service.
# 
# <em>servMove</em> is initialised as a matrix of zeros.
# 
# Go through the <em>DATA_SG</em> records by patient.  Use the current (j) <em>wardTeamCatCode</em> to set the row, and the next (j+1) wardTeamCatCode to set the column.  Increment the respective element in the <em>servMove</em> array (row: from service, column: to service) by +1.
# 
# The <em>servMove</em> array is written to an output file (location and filename is passed into the function by 5 elements of the dictionary) and also returned to the main code.
# 
# # EXERCISE 6
# 
# Create a vector (clientIDUni) that stores the unique ClientID values in the pandas dataframe that's stored in element 'DATA_SG' of the dictionary <em>file_output_info</em>.
# 

# In[ ]:


def output_SM_file(file_output_info):
    """Creates Service Movement numpy array from the Personality Disorder data
    The numpy array is used in function output_Edge_file() and also a csv file
    Recieves the PD data as a Pandas dataframe (in element DATA_SG), could 
    either be the whole network or a subgroup.
    The data is already grouped by patient and ordered chronologically on the 
    date the services they accessed (.ReferralDate). 
    Returns a NumPy array (servMove) with a row and column for each unique 
    WardTeam in the passed Pandas dataframe. 
    The values stored in the array are the frequency patients chronologically 
    used a service following another service."""
    
    #Number of wardteams
    n_wardteams = max(file_output_info['DATA_SG'].wardTeamCatCode) + 1

    #servMove is a matrix of zeros.
    #Number of columns and rows are the number of wardTeams (n_wardteams)
    #Each entry in servMove will record the frequency a patient 
    #chronologically used a service following another service
    servMove = np.zeros((n_wardteams, n_wardteams))

    #stores the number of clients that only have 1 service use 
    singles = np.zeros((1))  
    
    #EXERCISE 6
    #A vector of unique client ID's
    clientIDUni = file_output_info['DATA_SG'].ClientID.unique()    

    # loop through each unique ClientID
    for ID in clientIDUni:                              
        #mask for the rows that contain data for the ClientID
        mask = file_output_info['DATA_SG'].ClientID == ID
        #use the mask on the wardTeam column
        #return just the wardTeams for the specific ClientID
        cWardTeam = file_output_info['DATA_SG'][mask].wardTeamCatCode
        #number of services accessed by client
        n_services = len(cWardTeam)
        #if more than 1 service use, then can create an edge
        if (n_services > 1):
            #Go through each service use, until 2nd from last
            for j in range(0,(n_services - 1)):                    
                # Record the use from Source (this, j) to Target (next, j+1)
                servMove[int(cWardTeam.iloc[j]),int(cWardTeam.iloc[j + 1])] += 1 
        else:
            #If less than 1 service use, then record the client (debugging)
            singles = np.vstack((singles,ID))               

    #Create the output filename
    FileNameSM = (file_output_info['FILESTART'] + 
                  file_output_info['FILEMIDDLE'] + 
                  file_output_info['FILEENDSM'] + 
                  file_output_info['FILEEX'])

    #output service movement matrix as csv
    np.savetxt(file_output_info['FOLDER'] + FileNameSM, 
               servMove, delimiter = ",")
    return servMove


# Function <em>output_Edge_file()</em> is the second in a series of 3 functions that outputs a file.  Uses the numpy array <em>servMove</em> (created in function <em>output_servMove_file()</em>) to create and output the Edge file.
# 
# This function recieves the NumPy array <em>servMove</em> with a row and column for each unique WardTeam.  The values stored are the frequency a patient chronologically used a service following another service.
# 
# A NumPy array (<em>edges</em>) is initialised with a single row with 3 columns of zero values.  The code loops through each row of <em>servMove</em> (which is a WardTeam) and for any column with a value greater than zero, a row is added to the edges NumPy array to store the row number (source WardTeam ID) the colmun number (the target WardTeam ID) and the recorded value (the frequency of patients that have used that link).
# 
# The <em>edges</em> NumPy matrix is cleaned to remove the redundant first row used to initialise the matrix.
# The NumPy array (<em>edges</em>) now contains a row per link.  Convert to a Pandas dataframe and output as a csv file
# 
# The <em>edge</em> file is written to an output file (location and filename is passed into the function by 5 elements of the dictionary).
# 
# ## EXERCISE 7
# 
# Create a numpy array (edgetype) that stores the string "Directed" for the same number of rows that there are in the "edge" array.  
# 
# Hint.  Use np.repeat()

# In[ ]:


def output_Edge_file(servMove, file_output_info):
    """Creates EDGE file from the servMove NumPy array (output csv)
    Passesd the NumPy array servMove with row & column for each unique WardTeam. 
    The values are the frequency a patient chronologically used a service 
    following another service.
    For any value > 0 in servMove, a row in the EDGE file is created.
    Edge file has 5 columns:
    1) Source node ID [the servMove row] 
    2) Target node ID [the servMove column]
    3) Type [for this case, always DIRECTED]
    4) Edge ID [unique]
    5) Frequency of patient using the edge"""
    
    #initialise the numpy array (delete this redundant row later)
    edges = np.zeros((1,3))              
    #number of rows (the number of wardTeams)
    lenRow = servMove.shape[0]           
    #Through each row & column in the service movement matrix
    for i in range (0,lenRow):           
        for j in range(0,lenRow):
            #Record edge for any Source-Target combination that has been used
            if (int(servMove[j, i]) > 0):
                #rowData collates 3 values: SourceID, TargetID, Activity
                rowData = np.array([[j,i, int(servMove[j, i])]])
                #Add the row onto the numpy array
                edges = np.vstack((edges,rowData))                    

    #IDs to be integers
    edges=edges.astype(int)
    #clean up edges array: remove the superflous first row
    edges = edges[1:edges.shape[0], :]
    #number of rows (number of Source-Target combinations)
    lenEdge = edges.shape[0]
    
    #EXERCISE 7
    #create a numpy array (edgetype) that stores the string "Directed"
    #for the smae number of rows that are in the edge array
    #Change the string depending on whether a directed or undirected graph
    edgetype = np.repeat("Directed", lenEdge) 
    
    #Create a unique edgeid for the output file
    edgeid = np.arange(0, lenEdge)
    #join all output columns as numpy array
    edges = np.vstack((edges[:, 0], edges[:, 1], edgetype, edgeid, edges[:, 2]))
    #transpose the data (put data in columns)
    edges = np.transpose(edges)
    #create pandas dataframe from numpy array 'edges', specify column titles
    edgesdf = pd.DataFrame(edges, columns = 
                           ['Source', 'Target', 'Type', 'Id', 'Weight'])
    
    #Create the output filename
    FileNameEdge = (file_output_info['FILESTART'] + 
                    file_output_info['FILEMIDDLE'] + 
                    file_output_info['FILEENDEDGE'] + 
                    file_output_info['FILEEX'])

    #output edges dataframe as csv file
    edgesdf.to_csv(file_output_info['FOLDER'] + FileNameEdge, 
                   sep = ',', index = False)           
    return


# Function <em>output_Node_file()</em> is the third in a series of 3 functions that outputs a file.  Outputs the <em>Node</em> file.
# 
# This function gets passed a dictionary containing a Pandas dataframe of the PD data (in element <em>DATA_SG</em>), from which to create the network.  This data is grouped by patient and ordered chronologically on the date the services they accessed (.ReferralDate).
# 
# It creates a reference file for the nodes (WardTeams), a row per node.
# 
# Using the Pandas function "groupby" to calclate the mean and median LoS.
# 
# Creates a NumPy array (<em>nodes</em>) with a row per node storing the nodes ID, name, mean LoS, median Los, Setting.  The numpy array is converted to a Pandas dataframe.
# 
# The <em>node</em> Pandas dataframe is written to an output csv file (location and filename is passed into the function by 5 elements of the dictionary).
# 
# # EXERCISE 8
# 
# Remove any rows from dataframe <em>df</em> that have a duplicate in the combination of the columns: 'wardTeamCat', 'wardTeamCatCode', 'Setting'.
# Use an in place calculation that will drop the duplicates from the pandas dataframe df so that just the unique rows in terms of the 3 columns remain.

# In[ ]:


def output_Node_file(file_output_info):
    """Create NODE input file from the PD data (output a csv file)
    Passed the PD data as a Pandas dataframe in element 'DATA_SG' of the 
    directory 'file_output_info'.  Either for the whole network or a subgroup. 
    Creates a NumPy array containing a reference list for the nodes (WardTeams),
    a row per node.
    NumPy array is outputed as a csv file"""

    #Calculate mean and median LoS using pandas groupby function
    daysMeans = file_output_info['DATA_SG'].                groupby('wardTeamCatCode')['LoSdays'].mean()
    daysMedians = file_output_info['DATA_SG'].                groupby('wardTeamCatCode')['LoSdays'].median()

    #Getting unique Settings value per WardTeam
    #Create new dataframe, with values from 3 columns of 'DATA_SG'
    #Take a single case of occurence of WardTeamCat, Code & Setting
    df = pd.DataFrame()
    df['wardTeamCat'] = file_output_info['DATA_SG'].wardTeamCat
    df['wardTeamCatCode'] = file_output_info['DATA_SG'].wardTeamCatCode
    df['Setting'] = file_output_info['DATA_SG'].Setting

    # EXERCISE 8
    # Keep one case for each combination of wardTeamCat, wardTeamCatCode, Setting
    df.drop_duplicates(subset = ['wardTeamCat', 'wardTeamCatCode', 'Setting'], inplace = True)
    
    #Order the dataframe by the unique numerical wardTeam ID
    df.sort_values('wardTeamCatCode', inplace = True)

    #Join all output columns as numpy array
    nodes = np.vstack((df.wardTeamCatCode, df.wardTeamCat, 
                       daysMeans, daysMedians, df.Setting))
    #transpose the data (put data in columns)
    nodes = np.transpose(nodes)
    #create pandas dataframe from numpy array 'nodes', specify column titles
    nodesdf = pd.DataFrame(nodes,columns = 
                           ['ID', 'Label', 'MeanLoS', 'MedianLoS', 'Setting'])
    
    #Create the output filename
    FileNameNode = (file_output_info['FILESTART'] + 
                    file_output_info['FILEMIDDLE'] + 
                    file_output_info['FILEENDNODE'] + 
                    file_output_info['FILEEX'])

    #output nodes dataframe as csv file
    nodesdf.to_csv(file_output_info['FOLDER'] + FileNameNode, 
                   sep = ',', index = False)
    return


# ### Call the functions to create and output the files
# Now that all the functions are defined, can call the function (create_output_files) that calls the three functions in turn to create and output the datafiles

# In[ ]:


#Call the function that calls the 3 functions in turn that creates and 
#outputs the files for a network graph for this dataset.
create_output_files(file_output_info)


# ## Files for the whole data set complete!

# ## Next, create a network for a single patient
# 
# Use the same code (written as functions above) that was used for the Whole Network, in order create a network for the service use for an individual ClientID (1004961 and 1007835).
# 
# ### Using individual nodes for the Out Of Area Ward Teams
# The service use for a single patient is less complicated than for the whole dataset, keeping the OOA Ward Temas as individual nodes will not add too much noise to the graph.
# 
# The choice of pandas dataframe (DATA or DATA_OneOOA) is stored in dictionary <em>file_output_info</em>, element 'DATA'.
# 
# Dictionary <em>file_output_info</em> element 'FILEMIDDLE' stores a string to describe the dataset included in the output files.  For this case suggest including the string "_ClientID_" + their ID number, to represent the choice of representaing all the OOA ward teams as a single node.
# 
# Function <em>categorise_columns()</em> adds the unique numerical ID of Ward Teams to be used as the nodes reference ID.
# 
# Function <em>create_output_files()</em> calls the series of three functions to create the three output files.

# In[ ]:


clientID=[471,708]
for cID in clientID:
    mask = DATA.ClientID == cID
    #Deep copy. Changing DATA_SG does not change DATA
    file_output_info["DATA_SG"] = DATA[mask].copy(deep = True)
    file_output_info["FILEMIDDLE"] = '_ClientID_' + str(cID)

    #Adds the numerical representation of WardTeams present in the pandas dataframe 
    #Adds 2 columns: wardTeamCat & wardTeamCatCode
    file_output_info = categorise_columns(file_output_info)

    #Call the function that calls the 3 functions in turn to create and 
    #write the files for use in a Network graph
    create_output_files(file_output_info)

