#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 17:02:50 2019

@author: sean
"""

import networkx as nx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import community

#Create graph function
def create_graph(nodeData, edgeData):
    ## Initiate the graph object
    G = nx.Graph()
    
    ## Tranform the data into the correct format for use with NetworkX
    # Node tuples (ID, dict of attributes)
    idList = nodeData['Id'].tolist()
    labels =  pd.DataFrame(nodeData['Label'])
    labelDicts = labels.to_dict(orient='records')
    nodeTuples = [tuple(r) for r in zip(idList,labelDicts)]
    
    # Edge tuples (Source, Target, dict of attributes)
    sourceList = edgeData['Source'].tolist()
    targetList = edgeData['Target'].tolist()
    weights = pd.DataFrame(edgeData['weight'])
    weightDicts = weights.to_dict(orient='records')
    edgeTuples = [tuple(r) for r in zip(sourceList,targetList,weightDicts)]
    
    ## Add the nodes and edges to the graph
    G.add_nodes_from(nodeTuples)
    G.add_edges_from(edgeTuples)
    
    return G

#Read in data
nodesOne = pd.read_csv('data/got-s1-nodes.csv', low_memory=False)
edgesOne = pd.read_csv('data/got-s1-edges.csv', low_memory=False)

#Run the create_graph function
G_One = create_graph(nodesOne,edgesOne)

#Read in data
nodesTwo = pd.read_csv('data/got-s2-nodes.csv', low_memory=False)
edgesTwo = pd.read_csv('data/got-s2-edges.csv', low_memory=False)

#Run the create_graph function
G_Two = create_graph(nodesTwo,edgesTwo)

## degree
#Function to get node degrees and average degree
def degree_getter(graph):
    degreeDict = {}
    deg = graph.degree()
    degreeDict['degree'] = [d for n, d in deg]
    numNodes = len(degreeDict['degree'])
    degreeDict['avgDegree'] = sum(degreeDict['degree'])/numNodes
    
    return degreeDict

#Run the degree_getter function
deg_One = degree_getter(G_One)
deg_Two = degree_getter(G_Two)

# Plot and test mean difference
#Descriptive stats
deg_One_stats = stats.describe(deg_One['degree'])
deg_Two_stats = stats.describe(deg_Two['degree'])
print(deg_One_stats)
print(deg_Two_stats)
#Shapiro-Wilk test for normality
shap_test_One = stats.shapiro(deg_One['degree'])    #Null hyp: is normally distributed
shap_test_Two = stats.shapiro(deg_Two['degree'])    #Null hyp: is normally distributed
print(shap_test_One)
print(shap_test_Two)
#Levine test for homogeneity of variance
lev_test = stats.levene(deg_One['degree'],deg_Two['degree'])    #Null hyp: not homogeneous
print(lev_test)
#Mann-Whitney-U test 
mann_test = stats.mannwhitneyu(deg_One['degree'],deg_Two['degree'],
                               alternative='two-sided')    #Null hyp: Means are equal
print(mann_test)

#Create a list of arrays containing the degree data
d = [np.array(deg_One['degree']), np.array(deg_Two['degree'])]

#Create a boxplot of the degree data
plt.boxplot(d)

# modularity
# Determine the best partitions (modularity)
part_One = community.best_partition(G_One)
# Get the modular grouping designations for each node
val_One = [part_One.get(node) for node in G_One.nodes()]
# Draw the graph
pos_One = nx.spring_layout(G_One, k=0.5, iterations=20)
nx.draw(G_One, pos=pos_One, cmap = plt.get_cmap('jet'), node_color = val_One,
        edge_color='#AAB7B8', node_size=30, with_labels=False)
nx.draw_networkx_labels(G_One, pos_One, font_size=9)

# Determine the best partitions (modularity)
part_Two = community.best_partition(G_Two)
# Get the modular grouping designations for each node
val_Two = [part_Two.get(node) for node in G_Two.nodes()]
# Draw the graph
pos_Two = nx.spring_layout(G_Two, k=0.5, iterations=20)
nx.draw(G_Two, pos=pos_Two, cmap = plt.get_cmap('jet'), node_color = val_Two,
        edge_color='#AAB7B8', node_size=30, with_labels=False)
nx.draw_networkx_labels(G_Two, pos_Two, font_size=9)

#Get the overall modularity for each network
mod_One = community.modularity(part_One, G_One)
mod_Two = community.modularity(part_Two, G_Two)
print(mod_One)
print(mod_Two)

# eigenvector centrality
# Calculate the centrality of each node
cent_One = nx.eigenvector_centrality(G_One,max_iter=1000)
#Get the centrality values
cent_One_vals = list(cent_One.values())
#Inflate the centrality values by a scalar
cent_One_nsize = [x * 1000 for x in cent_One_vals]
#Draw the graph
nx.draw(G_One, pos=pos_One, cmap = plt.get_cmap('jet'), node_color = val_One,
        node_size=cent_One_nsize, edge_color='#AAB7B8', with_labels=False)
nx.draw_networkx_labels(G_One, pos_One, font_size=9)

# Calculate the centrality of each node
cent_Two = nx.eigenvector_centrality(G_Two,max_iter=1000)
#Get the centrality values
cent_Two_vals = list(cent_Two.values())
#Inflate the centrality values by a scalar
cent_Two_nsize = [x * 1000 for x in cent_Two_vals]
#Draw the graph
nx.draw(G_Two, pos=pos_Two, cmap = plt.get_cmap('jet'), node_color = val_Two,
        node_size=cent_Two_nsize, edge_color='#AAB7B8', with_labels=False)
nx.draw_networkx_labels(G_Two, pos_Two, font_size=9)

# clustering coefficient
clust_One = nx.clustering(G_One,weight='weight')
clust_Two = nx.clustering(G_Two, weight='weight')

## https://networkx.github.io/documentation/stable/reference/algorithms/index.html