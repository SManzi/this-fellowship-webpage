#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 13:58:48 2019

@author: sean
"""

import networkx as nx
import pandas as pd
import holoviews as hv
from holoviews import opts

nodes = pd.read_csv('data/got-s1-nodes.csv', low_memory=False)
edges = pd.read_csv('data/got-s1-edges.csv', low_memory=False)

## Initiate the graph object
G = nx.Graph()

## Tranform the data into the correct format for use with NetworkX
# Node tuples (ID, dict of attributes)
idList = nodes['Id'].tolist()
labels =  pd.DataFrame(nodes['Label'])
labelDicts = labels.to_dict(orient='records')
nodeTuples = [tuple(r) for r in zip(idList,labelDicts)]

# Edge tuples (Source, Target, dict of attributes)
sourceList = edges['Source'].tolist()
targetList = edges['Target'].tolist()
weights = pd.DataFrame(edges['weight'])
weightDicts = weights.to_dict(orient='records')
edgeTuples = [tuple(r) for r in zip(sourceList,targetList,weightDicts)]

## Add the nodes and edges to the graph
G.add_nodes_from(nodeTuples)
G.add_edges_from(edgeTuples)

## create the plot layout and draw the graph
## https://networkx.github.io/documentation/stable/reference/drawing.html
pos = nx.circular_layout(G) #circular_layout(G[, scale, center, dim]) Position nodes on a circle.
# pos = nx.spring_layout(G)   #spring_layout(G[, k, pos, fixed, …])   Position nodes using Fruchterman-Reingold force-directed algorithm.
# pos = nx.kamada_kawai_layout(G) #kamada_kawai_layout(G[, dist, pos, weight, …]) Position nodes using Kamada-Kawai path-length cost-function.
# pos = nx.random_layout(G)   #random_layout(G[, center, dim, seed])  Position nodes uniformly at random in the unit square.
# pos = nx.spectral_layout(G) #spectral_layout(G[, weight, scale, center, dim])   Position nodes using the eigenvectors of the graph Laplacian.
plot = nx.draw(G, pos=pos, node_size=100)

## Visualise the same data using holoviews

hv.extension('bokeh')
hv.output(size=300)

edgeList = edges[['Source','Target','weight']]
nodeDS = hv.Dataset(nodes,'Id')
chord = hv.Chord((edgeList, nodeDS))
chord.opts(
        opts.Chord(inspection_policy='nodes', tools=['hover'],
                   edge_hover_line_color='green', node_hover_fill_color='red'))
hv.save(chord,'vis.html')

## http://holoviews.org/getting_started/Customization.html
